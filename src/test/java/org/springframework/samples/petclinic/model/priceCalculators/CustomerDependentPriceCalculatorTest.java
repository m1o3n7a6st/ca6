package org.springframework.samples.petclinic.model.priceCalculators;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.samples.petclinic.model.Owner;
import org.springframework.samples.petclinic.model.Pet;
import org.springframework.samples.petclinic.model.PetType;
import org.springframework.samples.petclinic.model.UserType;
import org.springframework.samples.petclinic.rest.ExceptionControllerAdvice;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CustomerDependentPriceCalculatorTest {
    private List<Pet> pets;
    private  Owner owner;
    private  List<PetType> petTypes;
    @Mock
    PetType pettype;

    private CustomerDependentPriceCalculator cdpc;


    public void initPets(){

        Pet pet = new Pet();
        pet.setId(1);
        pet.setName("Rosy");
        pet.setBirthDate(new Date());
        pet.setOwner(owner);
        pet.setType(petTypes.get(1));
        pets.add(pet);

        pet = new Pet();
        pet.setId(2);
        pet.setName("Jewel");
        pet.setBirthDate(diffyear(-2));
        pet.setOwner(owner);
        pet.setType(petTypes.get(0));
        pets.add(pet);

        pet = new Pet();
        pet.setId(3);
        pet.setName("Jewel");
        pet.setBirthDate(diffyear(-2));
        pet.setOwner(owner);
        pet.setType(petTypes.get(0));
        pets.add(pet);


    }

    public Date diffyear(int year){
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        cal.add(Calendar.YEAR, year);
        return  cal.getTime();

    }
    public void generate_pet(int year,int offset,Boolean rate){
        for(int i= 0 ; i<10 ; i++){
            Pet pet= new Pet();
            pet.setId(i+offset);
            pet.setName("hi");
            pet.setBirthDate(diffyear(year));
            pet.setOwner(owner);
            if (rate){
                pet.setType(petTypes.get(i%2));
            }
            else {
                pet.setType(petTypes.get(2));
            }

            pets.add(pet);

        }
    }

    @Before
    public void setUp() {

        pets = new ArrayList<>();
        petTypes = new ArrayList<>();
        owner = new Owner();
        owner.setId(1);
        owner.setFirstName("Eduardo");
        owner.setLastName("Rodriquez");
        owner.setAddress("2693 Commerce St.");
        owner.setCity("McFarland");
        owner.setTelephone("6085558763");

        PetType petType = mock(PetType.class);
        when(petType.getRare()).thenReturn(false);

        PetType petType1 = mock(PetType.class);
        when(petType1.getRare()).thenReturn(true);

        PetType petType2 = mock(PetType.class);
        when(petType2.getRare()).thenReturn(true);

        petTypes.add(petType1);
        petTypes.add(petType2);
        petTypes.add(petType);

        cdpc = new CustomerDependentPriceCalculator();




    }

    @After
    public void tearDown()  {
    }

    @Test
    public void calcPrice_kids_newuser() {
        initPets();
        generate_pet(0,2,true);
        UserType myUser = UserType.NEW;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  46.4, num,0.1);
    }

    @Test
    public void calcPrice_kids_newuser_notrate() {
        initPets();
        generate_pet(0,2,false);
        UserType myUser = UserType.NEW;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  37.3, num,0.1);
    }

    @Test
    public void calcPrice_kids_Golduser() {
        initPets();
        generate_pet(0,2,true);
        UserType myUser = UserType.GOLD;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  38.9, num,0.1);
    }
    @Test
    public void calcPrice_kids_Golduser_notrate() {
        initPets();
        generate_pet(0,2,false);
        UserType myUser = UserType.GOLD;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  31.2, num,0.1);
    }
    @Test
    public void calcPrice_adult_kids_newuser() {
        initPets();
        generate_pet(-2,2,true);
        generate_pet(-2,14,true);
        UserType myUser = UserType.NEW;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  78.4, num,0.1);

    }
    @Test
    public void calcPrice_adult_kids_newuser_notrateandrate() {
        initPets();
        generate_pet(-2,2,false);
        generate_pet(-2,14,true);
        UserType myUser = UserType.NEW;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  69.2, num,0.1);

    }
    @Test
    public void calcPrice_adult_kids_newuser_notrate() {
        initPets();
        generate_pet(-2,2,false);
        generate_pet(-2,14,false);
        UserType myUser = UserType.NEW;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  60.1, num,0.1);

    }
    @Test
    public void calcPrice_adult_kids_Golduser() {
        initPets();
        generate_pet(-2,2,true);
        generate_pet(-2,14,true);
        UserType myUser = UserType.GOLD;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  65.8, num,0.1);

    }
    @Test
    public void calcPrice_adult_kids_Golduser_notRate() {
        initPets();
        generate_pet(-2,2,false);
        generate_pet(-2,14,false);
        UserType myUser = UserType.GOLD;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  50.4, num,0.1);

    }

    @Test
    public void calcPrice_adult_newuser() {
        generate_pet(-2,2,true);
        generate_pet(-3,14,true);
        UserType myUser = UserType.NEW;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  59.7, num,0.1);

    }
    @Test
    public void calcPrice_adult_newuser_notrate() {
        generate_pet(-2,2,false);
        generate_pet(-3,14,false);
        UserType myUser = UserType.NEW;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  46.8, num,0.1);

    }

    @Test
    public void calcPrice_adult_Golduser() {
        generate_pet(-2,2,true);
        generate_pet(-3,14,true);
        UserType myUser = UserType.GOLD;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  50.0, num,0.1);

    }
    @Test
    public void calcPrice_adult_Golduser_notrate() {
        generate_pet(-2,2,false);
        generate_pet(-3,14,false);
        UserType myUser = UserType.GOLD;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  39.2, num,0.1);

    }


    @Test
    public void calcPrice_adult_kids_under10pets_newuser() {
        initPets();
        UserType myUser = UserType.NEW;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  10.0, num,0.1);
    }
    @Test
    public void calcPrice_adult_kids_under10pets_newuser_notrate() {
        Pet pet = new Pet();
        pet.setId(1);
        pet.setName("Rosy");
        pet.setBirthDate(new Date());
        pet.setOwner(owner);
        pet.setType(petTypes.get(2));
        pets.add(pet);
        UserType myUser = UserType.NEW;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  2.4, num,0.1);
    }
    @Test
    public void calcPrice_adult_kids_under10pets_Goldser() {
        initPets();
        UserType myUser = UserType.GOLD;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  13.0, num,0.1);
    }
    @Test
    public void calcPrice_adult_kids_under10pets_Goldser_notRate() {
        Pet pet = new Pet();
        pet.setId(1);
        pet.setName("Rosy");
        pet.setBirthDate(new Date());
        pet.setOwner(owner);
        pet.setType(petTypes.get(2));
        pets.add(pet);
        UserType myUser = UserType.GOLD;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  6.92, num,0.1);
    }

    @Test
    public void nullPet() {
        UserType myUser = UserType.GOLD;
        double num=cdpc.calcPrice(pets,5,2,myUser);
        assertEquals(  5, num,0.1);
    }





}
